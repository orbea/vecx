#ifndef E6809_H
#define E6809_H

typedef struct cpust_t {
    uint16_t reg_x;
    uint16_t reg_y;
    uint16_t reg_u;
    uint16_t reg_s;
    uint16_t reg_pc;
    uint8_t reg_a;
    uint8_t reg_b;
    uint8_t reg_dp;
    uint8_t reg_cc;
    uint8_t irq_status;
} cpust_t;

// user defined read and write functions
extern uint8_t (*e6809_read8)(uint16_t);
extern void (*e6809_write8)(uint16_t, uint8_t);

void e6809_reset(void);
uint16_t e6809_sstep(uint16_t, uint16_t);

void vecx_cpu_state_load(cpust_t*);
void vecx_cpu_state_save(cpust_t*);

#endif
