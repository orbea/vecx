#ifndef VECX_H
#define VECX_H

#define EMU_TIMER 20 // the emulator's heart beats at 20 milliseconds
#define VECTREX_MHZ 1500000 // speed of the vectrex being emulated
#define VECTREX_COLORS 128 // number of possible colors ... grayscale
#define ALG_MAX_X 33000
#define ALG_MAX_Y 41000

#define SIZE_BIOS 8192

enum {
    VECTREX_PDECAY  = 30,      /* phosphor decay rate */

    /* number of 6809 cycles before a frame redraw */
    FCYCLES_INIT    = VECTREX_MHZ / VECTREX_PDECAY,

    /* max number of possible vectors that maybe on the screen at one time.
       one only needs VECTREX_MHZ / VECTREX_PDECAY but we need to also store
       deleted vectors in a single table
    */
    VECTOR_CNT      = VECTREX_MHZ / VECTREX_PDECAY,

    VECTOR_HASH     = 65521
};

typedef struct vector_type {
    int32_t x0, y0; // start coordinate
    int32_t x1, y1; // end coordinate
    /* color [0, VECTREX_COLORS - 1], if color = VECTREX_COLORS, then this is
     * an invalid entry and must be ignored.
     */
    uint8_t color;
} vector_t;

extern uint16_t alg_jch0;
extern uint16_t alg_jch1;
extern uint16_t alg_jch2;
extern uint16_t alg_jch3;

extern int32_t vector_draw_cnt;
extern int32_t vector_erse_cnt;
extern vector_t *vectors_draw;
extern vector_t *vectors_erse;

int vecx_bios_load_file(const char*);
int vecx_bios_load(void*, size_t);
int vecx_rom_load(void*, size_t);

void vecx_reset(void);
void vecx_emu(int32_t);

void vecx_deinit(void);

size_t vecx_state_size(void);

void vecx_state_load_raw(const void *sstate);
int vecx_state_load(const char *filename);

const void* vecx_state_save_raw(void);
int vecx_state_save(const char *filename);

#endif
