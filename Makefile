SOURCEDIR := $(abspath $(patsubst %/,%,$(dir $(abspath $(lastword \
	$(MAKEFILE_LIST))))))

NAME := vecx
JGNAME := $(NAME)-jg

SRCDIR := $(SOURCEDIR)/src

INCLUDES =
INCLUDES_JG = -I$(SRCDIR)

LIBS = -lm

LIBS_REQUIRES := speexdsp

DOCS := LICENSE README

# Object dirs
MKDIRS :=

override INSTALL_DATA := 1
override INSTALL_EXAMPLE := 0
override INSTALL_SHARED := 0

include $(SOURCEDIR)/version.h
include $(SOURCEDIR)/mk/jg.mk

INCLUDES += $(CFLAGS_SPEEXDSP)
LIBS += $(LIBS_SPEEXDSP)

EXT := c
FLAGS := -std=c99 $(WARNINGS_DEF_C)
LINKER := $(CC)

CSRCS := e6809.c \
	vecx.c \
	vecx_mixer.c \
	vecx_psg.c \
	vecx_render.c \
	vecx_serial.c

JGSRCS := jg.c

# Assets
DATA := rom.dat

DATA_TARGET := $(DATA:%=$(NAME)/%)

# List of object files
OBJS := $(patsubst %,$(OBJDIR)/%,$(CSRCS:.c=.o) $(OBJS_SPEEXDSP))
OBJS_JG := $(patsubst %,$(OBJDIR)/%,$(JGSRCS:.c=.o))

# Core commands
BUILD_JG = $(call COMPILE_C, $(FLAGS) $(INCLUDES_JG) $(CFLAGS_JG))
BUILD_MAIN = $(call COMPILE_C, $(FLAGS) $(INCLUDES))

.PHONY: $(PHONY)

all: $(TARGET)

# Rules
$(OBJDIR)/%.o: $(SRCDIR)/%.$(EXT) $(PREREQ)
	$(call COMPILE_INFO,$(BUILD_MAIN))
	@$(BUILD_MAIN)

# Data rules
$(DATA_TARGET): $(DATA:%=$(SOURCEDIR)/%)
	@mkdir -p $(NAME)
	@cp $(DATA:%=$(SOURCEDIR)/%) $(NAME)/

install-data: all
	@mkdir -p $(DESTDIR)$(DATADIR)/jollygood/$(NAME)
	cp $(NAME)/rom.dat $(DESTDIR)$(DATADIR)/jollygood/$(NAME)/

include $(SOURCEDIR)/mk/rules.mk
